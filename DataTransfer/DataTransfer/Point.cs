﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace DataTransfer
{
    /// <summary>
    /// Данные о точке для построения графика на клиенте
    /// </summary>
    [DataContract]
    public class Point
    {
        [DataMember]
        public int x { get; set; }
        [DataMember]
        public int y { get; set; }

        public Point(int x, int y)
        {
            this.x = x;
            this.y = y;
        }
    }
}
